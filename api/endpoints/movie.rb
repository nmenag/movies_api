# frozen_string_literal: true

module Api
  module Endpoints
    class Movie < Grape::API
      namespace :movie do
      end
    end
  end
end
